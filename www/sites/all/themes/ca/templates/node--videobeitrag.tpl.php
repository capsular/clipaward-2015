<article<?php print $attributes; ?>>
  <?php print $user_picture; ?>
  <?php print render($title_prefix); ?>
  <?php if (!$page && $title): ?>
  <header>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  </header>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <div<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
   
  </div>
  
  <div class="clearfix">
    <?php 
        $user_item = user_load( $node->uid );
        $profile = profile2_load_by_user($user_item, 'main');
    ?>
    <?php if ($display_submitted): ?>
        <footer class="submitted clearfix">
        
					<div id="udata" class="float">
						<div class="float-inner">
							<?php print t("Aufgeladen am ") . $mydate . ","; ?><br />
							<?php print t("von ") . $profile->field_vorname['und'][0]['safe_value']; ?> <?php print $profile->field_nachname['und'][0]['safe_value']; ?> (<?php print $profile->field_alter['und'][0]['value']; ?>)
						</div>
					</div>
					<div id="fblink" class="float">	
						<div class="float-inner">
							<div class="fb-like" data-layout="button_count" data-send="false" data-show-faces="false" data-width="200"></div>
						</div>
					</div>
					<?php
            print '<div id="votebox" class="float">';
            print $votinglink; 
            print '</div>';
          ?>
        </footer>
    <?php endif; ?>  
    
    <?php if (!empty($content['links'])): ?>
      <nav class="links node-links clearfix"><?php print render($content['links']); ?></nav>
    <?php endif; ?>

    <?php print render($content['comments']); ?>
  </div>
</article>