<?php print $doctype; ?>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf->version . $rdf->namespaces; ?>>
<head<?php print $rdf->profile; ?>>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>  
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body<?php print $attributes;?>>
  <div id="fb-root"></div>
  <script>

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  <?php if($language->language == "de" ) : ?>
  js.src = "//connect.facebook.net/de_DE/sdk.js#xfbml=1&appId=352983331427562&version=v2.0";
  <?php elseif($language->language == "fr" ) : ?>
  js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&appId=352983331427562&version=v2.0";
  <?php endif; ?>
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

     <script>
    // window.fbAsyncInit = function() {
    //   // init the FB JS SDK
    //   FB.init({
    //     appId      : '352983331427562',                        // App ID from the app dashboard
    //     <?php if($language->language == "de" ) : ?>
    //       channelUrl : '//www.clipaward.ch/channel_de.php',
    //     <?php elseif($language->language == "fr" ) : ?>
    //       channelUrl : '//www.clipaward.ch/channel_fr.php',
    //     <?php else : ?>
    //       channelUrl : '//www.clipaward.ch/channel_en.php',
    //     <?php endif; ?>
    //     status     : true,                                 // Check Facebook Login status
    //     xfbml      : true                                  // Look for social plugins on the page
    //   });

    //   // Additional initialization code such as adding Event Listeners goes here
    // };

    // Load the SDK asynchronously
    // (function(d, s, id){
    //     var js, fjs = d.getElementsByTagName(s)[0];
    //     if (d.getElementById(id)) {return;}
    //     js = d.createElement(s); js.id = id;
    //     <?php if($language->language == "de" ) : ?>
    //       js.src = "//connect.facebook.net/de_DE/all.js";
    //     <?php elseif($language->language == "fr" ) : ?>
    //       js.src = "//connect.facebook.net/fr_FR/all.js";
    //     <?php else : ?>
    //       js.src = "//connect.facebook.net/en_EN/all.js";
    //     <?php endif; ?>
    //     fjs.parentNode.insertBefore(js, fjs);
    //     }(document, 'script', 'facebook-jssdk'));
    </script>
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-39260740-2', 'clipaward.ch');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');
 
</script>
</body>
</html>