

<?php global $language; global $base_url; ?>
<?php // dsm($votinglink); ?>
<div class="<?php print $classes; ?> clearfix">
    <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
      <?php $i=1; $class=""; ?>
      <?php foreach ($view->result as $result): ?>
      <?php 
            $path = drupal_lookup_path('alias',"node/".$result->nid);
        ?>
        <?php if($i%3 == 1) {
            $class="first"; } elseif($i%3 == 0) {
            $class="last"; } else {$class='';} ?>
        <div class="ytitem<?php echo ' ' . $class; ?>">
        	<?php //if( (int)$result->field_field_alter[0]['raw']['value'] < 20 ) : ?>
        		<?php // print '<div class="u21">U20</div>'; ?>
        	<?php // endif; ?>
        	<div class="ytimg">
        		<?php print '<a href="' . $base_url . base_path() . $language->language . '/' . $path . '">'; ?>
        		<?php print render($result->field_field_clip) ?>
        		</a>
        	</div>
        	<div class="titleandname">
						<h4><?php print $result->node_title; ?></h4>
						<?php 
							$user_item = user_load( $result->_field_data['nid']['entity']->uid );
							$profile = profile2_load_by_user($user_item, 'main');
						?>
						<div class="ytmeta">
							<?php print $profile->field_vorname['und'][0]['safe_value'] . " " . $profile->field_nachname['und'][0]['safe_value']; ?>
						</div>
					</div>
        	<?php print($votinglink[$i-1]); ?>
        </div>
        
        <?php $i++;  ?>
      <?php endforeach; ?>
  <?php endif; ?>


</div><?php /* class view */ ?>
