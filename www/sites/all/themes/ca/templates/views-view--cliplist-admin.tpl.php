<?php
/**
 * @file views-view.tpl.php
 * Main view template
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>

<?php global $language; ?>

<div class="<?php print $classes; ?> clearfix">
    
    <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
      <?php foreach ($view->result as $result): ?>
      <?php 
            $path = drupal_lookup_path('alias',"node/".$result->nid);
            // dsm($result);
        ?>

        <div class="yt" style="margin-bottom:2em">
        
        <div class="ytimg">
        <?php print '<a href="http://www.clipaward.ch/' . $language->language . '/' . $path . '">'; ?>
        <img src="http://img.youtube.com/vi/<?php print $result->field_field_ytupload[0]['raw']['video_id']; ?>/0.jpg">
        </a>
        </div>
        <h6 style="margin:0;padding:0"><?php print $result->node_title; ?></h6>
        <div>
            <?php print $result->field_field_vorname[0]['raw']['safe_value'] . " " . $result->field_field_nachname[0]['raw']['safe_value']; ?> 
            / Alter: <?php print render($result->field_field_alter); ?>
            <br />
            <?php print render($result->field_field_strasse); ?>
             / <?php print render($result->field_field_ort); ?>
            <br /><?php print $result->users_node_mail; ?>
        </div>
        </div>
      <?php endforeach; ?>
  <?php endif; ?>


</div><?php /* class view */ ?>
