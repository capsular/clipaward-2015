<?php



function ca_js_alter(&$js) { 
    unset($js[drupal_get_path('module','logintoboggan').'/logintoboggan.unifiedlogin.js']); 
}


function ca_preprocess_page(&$vars) { 
    global $language;
    $lang = $language->language;
//     if($vars['is_front']) {
//         
//         if ($lang == "fr") {
//             drupal_goto('frontpage');
//         } else {
//             drupal_goto('frontpage');
//         }
//         
//     }
    
    drupal_add_js(drupal_get_path('theme', 'ca') .'/js/jquery.cycle2.min.js', 
            array('type' => 'file', 'scope' => 'header', 'weight' => 70) 
        );
    // drupal_add_js(drupal_get_path('theme', 'ca') .'/js/home.js', 
    //         array('type' => 'file', 'scope' => 'header', 'weight' => 80) 
    //     );
    drupal_add_js(drupal_get_path('theme', 'ca') .'/js/validation.js', 
            array('type' => 'file', 'scope' => 'header', 'weight' => 90) 
        );
    drupal_add_js(drupal_get_path('theme', 'ca') .'/js/jquery.backstretch.js', 
            array('type' => 'file', 'scope' => 'header', 'weight' => 65) 
        );
    if($lang == "fr") {
        drupal_add_js('errormsges = ["Il faut inserer une adresse émail valide!"]', 
            array('type' => 'inline', 'scope' => 'header', 'weight' => 100) 
        );
    } else {
        drupal_add_js('errormsges = ["Bitte gültige E-Mail-Adresse eingeben!"];', 
            array('type' => 'inline', 'scope' => 'header', 'weight' => 100) 
        );
    }

    //   $bgimg = "/sites/all/themes/ca/images/2014_bg001.jpg"; // default bg
    //   $versatz = -50;

    // if(array_key_exists('node', $vars)) {
    //   $node_id = $vars['node']->nid;
    //   if($node_id == 3) { // thema
    //     $bgimg = "/sites/all/themes/ca/images/2014_bg002a.jpg";
    //     $versatz = 300;
    //   } else if($node_id == 2) { // faq
    //     $bgimg = "/sites/all/themes/ca/images/2014_bg003a.jpg";
    //     $versatz = 200;
    //   } else if($node_id == 5) { // preise
    //     $bgimg = "/sites/all/themes/ca/images/2014_bg004.jpg";
    //     $versatz = false;
    //   } else if($node_id == 184) { // jury
    //     $bgimg = "/sites/all/themes/ca/images/2014_bg005.jpg";
    //     $versatz = false;
    //   } 
    // }
    // if($versatz) {
    //   drupal_add_js('jQuery(document).ready(function () { jQuery.backstretch("' . $bgimg . '", {versatz:' . $versatz . '}) });',
    //     array('type' => 'inline', 'scope' => 'footer', 'weight' => 5)
    //   );
    // } else {
    //   drupal_add_js('jQuery(document).ready(function () { jQuery.backstretch("' . $bgimg . '") });',
    //     array('type' => 'inline', 'scope' => 'footer', 'weight' => 5)
    //   );
    // }
    
    
}

function ca_page_alter(&$build) { 
    if(isset($build['content']['content']['content']['system_main']['nodes'])){
        $array = $build['content']['content']['content']['system_main']['nodes'];
        reset($array);
        $first_key = key($array);
        $node = $array[$first_key];
        if($node['#bundle'] == 'videoclip') {
            $build['content']['content']['content']['system_main']['vlist'] = array('#markup'=>views_embed_view('cliplist','page'));
        }
    }

    // if($vars['type'] == 'videobeitrag') {
       //  $vars['videolist'] = array('#markup'=>views_embed_view('cliplist','page'));
    // };
}

function _votingString($nid, $loggedin=false, $frompage = 'cliplist' ) {
  global $language;
  $lang = $language->language;
	$votingarray = flag_get_counts('node', $nid);
	$votinglink = '<div class="voteblock">';
	if(array_key_exists('voting',$votingarray)) { 
			$votingvotes = $votingarray['voting'];
	} else {
		$votingvotes = 0;
	}
	$votinglink .= '<div class="votecount float vlink">';

	if($votingvotes != 1){
			$votestring = t("Votes");
	} else {
			$votestring = t("Vote");
	}
	$votinglink .= '<span class="votenr">' . $votingvotes . '</span>';
	$votinglink .= ' ' . $votestring . '</div>';


	// $votinglink .= '<div class="votelink float vlink">';
	// if($loggedin) {
	// 	$votinglink .= flag_create_link('voting', $nid);
	// } else {
 //    if($lang == "fr") {
	// 	  $votinglink .= '<a class="" href="/fr/voting/register?destination=' . $frompage . '">' . t("Vote") . '</a>';
 //    } else {
 //      $votinglink .= '<a class="" href="/voting/register?destination=' . $frompage . '">' . t("Vote") . '</a>';
 //    }
	// }
	//$votinglink .= '</div>';


  $votinglink .= '</div>';
	return $votinglink;
}

function ca_preprocess_node(&$vars) {
    
    if($vars['type'] == 'videoclip') {
       $alias = drupal_get_path_alias('node/' . $vars['nid']);
       $vars['votinglink'] = _votingstring($vars['nid'], user_is_logged_in(), $alias );
       $vars['mydate'] = format_date($vars['node']->created, 'short');
    }
    
}

function ca_preprocess_views_view(&$vars) {
     if ( $vars['view']->name == 'cliplist' ) {
         $nid_array = '';
         foreach($vars['view']->result as $item){
         		 $nid_array[] = _votingstring($item->nid, user_is_logged_in());
         };
         $vars['votinglink'] = $nid_array;
         
     }
    

}

function ca_preprocess_html(&$vars) { 
//     global $language ;
//     
//     $fbapptag = array(
//     		'#type' => 'html_tag',
//     		'#tag' => 'meta',
//     		'#attributes' => array(
//       		'property' => 'fb:app_id', 
// 			'content' => '352983331427562'
//     		));
//     $fbadmintag = array(
//     		'#type' => 'html_tag',
//     		'#tag' => 'meta',
//     		'#attributes' => array(
//       		'property' => 'fb:admins', 
// 			'content' => '1557892916'
//     		));
// 		//Add meta tags to head	
// 		//drupal_add_html_head($fbapptag, 'fbapptag');
// 		drupal_add_html_head($fbadmintag, 'fbadmintag');
}

function ca_lt_unified_login_page($variables) {
  $login_form = $variables['login_form'];
  $register_form = $variables['register_form'];
  $active_form = $variables['active_form'];
  $output = '';

  $output .= '<div class="toboggan-unified ' . $active_form . '">';

  // Create the initial message and links that people can click on.
  $output .= '<div id="login-message">' . t('You are not logged in.') . '</div>';
  $output .= '<div id="login-links">';
  $output .= l(t('I have an account'), 'user/login', array('attributes' => array('class' => array('login-link'), 'id' => 'login-link')));
  $output .= ' ';
  $output .= l(t('I want to create an account'), 'user/register', array('attributes' => array('class' => array('login-link'), 'id' => 'register-link')));

  $output .= '</div>';

  // Add the login and registration forms in.
  $output .= '<div id="login-form">';
  $output .= '<h3>' . t("Log in") . '</h3>';
  $output .= $login_form . '</div>';
  $output .= '<div id="register-form">';
  $output .= '<h3>' . t("Register") . '</h3>';
  $output .= $register_form . '</div>';

  $output .= '</div>';

  return $output;
}

 function ca_links__locale_block($vars) {
    // dsm($vars);
    $output = '';
    if( array_key_exists("href",$vars["links"]["de"]) ) {    
        $output = l("DE", $vars["links"]["de"]["href"], $vars["links"]["de"]);
    }
    if( array_key_exists("href",$vars["links"]["fr"]) ) {
        $output .= ' &nbsp;| &nbsp;' . l("FR", $vars["links"]["fr"]["href"], $vars["links"]["fr"]);
    }
    return $output; 
}

function ca_form_alter(&$form, &$form_state, $form_id) {
    // kpr($form);
    switch ($form_id) {
      case 'videoclip_node_form' :
        global $language;
        $form['language']['#printed'] = true;
        $form['field_clip']['und'][0]['#title'] = "Clip-Upload";
        $form['actions']['submit']['#submit'][] = 'ca_submit';
      break;
      case 'media_internet_add' :
      break;
      case 'user_register_form' :
      	$form['#prefix'] = "<p id='regprefix'>" . t("Um deine Stimme abzugeben, musst du dich registrieren. Nach dem Absenden dieses Formulars erhältst du von uns ein E-Mail mit einem Link zur Bestätigung deiner E-Mailadresse. ");
      	$form['#prefix'] .= t("Wenn du bereits ein Benutzerkonto hast, logge dich mit dem 'Anmelden'-Link direkt über diesem Text ein.");
        if(array_key_exists('profile_main', $form)) {
            global $language;
            if($language->language == 'de') {
                $form['profile_main']['field_wb']['und']['#title'] = 'Ich akzeptiere die <span id="accept">' . $form['profile_main']['field_wb']['und']['#title'] . '</span>';
            } else {
                $form['profile_main']['field_wb']['und']['#title'] = 'J&#39;accepte les <span id="accept">' . $form['profile_main']['field_wb']['und']['#title'] . '</span>';
            }
        }
        $form['#attached']['js']= array(drupal_get_path('theme', 'ca') . '/js/agb.js');
      break;
    }
}

function ca_submit($form, &$form_state) {
    global $language;
    if($language->language == 'de') {
        drupal_set_message('Vielen Dank für Deinen Wettbewerbsbeitrag! Dein Film wird nun bearbeitet und ist in spätestens einer Viertelstunde auf der Website zu sehen');
        $form_state['redirect'] = '<front>';
        // node_form_submit($form, $form_state);
    } else {
        drupal_set_message("Merci pour ta participation au concours! Ton film sera visible sur le site au plus tard dans 15 minutes.");
        $form_state['redirect'] = '<front>';
    }
    
}
