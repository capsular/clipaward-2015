(function ($) {

    $(function() {
        $("#accept").click(function(){
            $el = $(this).parent().next()
            if($el.is(':visible')){
                $el.slideUp('fast')
            } else {
                $el.slideDown('fast')
            }
        })
    
    });
  
})(jQuery);